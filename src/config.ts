import { hasEnv, getEnv } from "./utils/env";

if (!hasEnv("METADATA_URL")) {
  console.error("METADATA_URL is required");
  process.exit(1);
}

export default {
  keycloak: {
    metadataURL: getEnv("METADATA_URL")!,
  },
  server: {
    host: getEnv("HOST", "0.0.0.0")!,
    port: Number(getEnv("PORT")) || 8000,
  },
};
