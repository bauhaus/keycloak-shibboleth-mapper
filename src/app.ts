import express, { Request, Response } from "express";
import morgan from "morgan";

import { fetchMetadata, mapMetadata } from "./utils/metadata";

const app = express();

app.use(morgan("combined"));

app.get("/", async (req: Request, res: Response) => {
  const meta = await fetchMetadata();
  const mappedMeta = await mapMetadata(meta);

  res.contentType("application/xml;charset=UTF-8");
  res.send(mappedMeta);
});

export default app;
