export function hasEnv(name: string): boolean {
  return process.env[name] !== undefined;
}

export function getEnv(
  name: string,
  defaultValue?: string
): string | undefined {
  if (!hasEnv(name)) {
    return defaultValue;
  }

  return process.env[name];
}
