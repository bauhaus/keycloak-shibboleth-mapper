import fetch from "node-fetch";
import xml2js from "xml2js";

import config from "../config";
import { hasEnv, getEnv } from "./env";

export async function fetchMetadata(): Promise<string> {
  return (await fetch(config.keycloak.metadataURL)).text();
}

export function buildUIInfo(tag: string): any {
  const obj: any = {};
  obj[`mdui:${tag}`] = [
    buildUIInfoWithLang(tag, "de"),
    buildUIInfoWithLang(tag, "en"),
  ].filter((x) => !!x);

  if (obj[`mdui:${tag}`].length === 0) {
    return {};
  }

  return obj;
}

export function buildUIInfoWithLang(tag: string, lang: string): any {
  const LANG = lang.toUpperCase();
  const TAG = tag.toUpperCase();

  if (!hasEnv(`${TAG}_${LANG}`)) {
    return;
  }

  return {
    $: {
      "xml:lang": lang,
    },
    _: getEnv(`${TAG}_${LANG}`),
  };
}

export function buildUIInfoLogo(): any {
  if (hasEnv("LOGO_URL")) {
    return {
      "mdui:Logo": {
        $: {
          width: getEnv("LOGO_WIDTH"),
          height: getEnv("LOGO_HEIGHT"),
        },
        _: getEnv("LOGO_URL"),
      },
    };
  } else {
    return {};
  }
}

export async function mapMetadata(meta: string): Promise<string> {
  const parsed = await xml2js.parseStringPromise(meta);

  parsed["md:EntityDescriptor"]["$"]["xmlns:mdui"] =
    "urn:oasis:names:tc:SAML:metadata:ui";

  parsed["md:EntityDescriptor"]["md:SPSSODescriptor"][0]["md:Extensions"] = {
    "mdui:UIInfo": {
      ...buildUIInfo("DisplayName"),
      ...buildUIInfo("Description"),
      ...buildUIInfo("InformationURL"),
      ...buildUIInfo("PrivacyStatementURL"),
      ...buildUIInfoLogo(),
    },
  };

  const builder = new xml2js.Builder();
  return builder.buildObject(parsed);
}
